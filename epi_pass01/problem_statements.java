 // Problem 6.0
 /**
 * Your input is an array of integers, and you have to reorder its entries so that the even entries appear first. This
 * is easy if you use 0(n) space, where n is the length of the array. However, you are required to solve it without
 * allocating additional storage.
 */

 // Problem 6.1
 /**
 * The Dutch National Flag problem
 * Write a program that takes an array A and an index i into A, and rearranges the
 * elements such that all elements less than A[i] (the "pivot") appear first, followed by
 * elements equal to the pivot, followed by elements greater than the pivot.
 */

 // Problem 6.2
 /**
 * Write a program which takes as input an array of digits encoding a decimal number
 * D and a integer n and updates the array to represent the number D + n
 */

 // Problem 6.2.1
 /**
 * Write a program which takes as input two strings s and t of bits encoding
 * binary numbers Bs and Bt, respectively, and returns a new string of bits representing
 * the number Bs + Bt.
 */

 // Problem 6.3 (Not done)
 /**
 *  Write a program that takes two arrays representing integers, and returns an integer representing their product.
 */

 // Problem 6.5
 /**
 * Write a program which takes as input a sorted array and updates it so that all duplicates have been removed and the
 * remaining elements have been shifted left to fill the emptied indices. Return the number of valid elements.
 */

 // Problem 6.5.1
 /**
 * Implement a function which takes as input an array and a key and updates
 * the array so that all occurrences of the input key have been removed and the remaining
 * elements have been shifted left to fill the emptied indices. Return the number of
 * remaining elements. There are no requirements as to the values stored beyond the
 * last valid element.
 */

 // Problem 6.5.2 (Not done)
 /**
 * Write a program which takes as input a sorted array A of integers and a
 * positive integer m, and updates A so that if x appears m times in A it appears exactly
 * min(2,m) times in A. The update to A should be performed in one pass, and no
 * additional storage may be allocated.
 */

 // Problem 6.6
 /**
 * Write a program that takes an array denoting the daily stock price, and returns the
 * maximum profit that could be made by buying and then selling one share of that
 * stock.
 */

 // Problem 6.6.1


 // Problem 7.3
 /**
 * Implement a function that converts a spreadsheet column id to the corresponding
 * integer, with "A" corresponding to 1. For example, you should return 4 for "D", 27
 * for "AA", 702 for "ZZ", etc.
 */

 // Problem 7.3.1
 /**
 * Implement a function that converts an integer to the spreadsheet column id. For example, you
 * should return "D" for 4, "AA" for 27, and "ZZ" for 702
 */

 // Problem 7.8
 /**
 * The look-and-say sequence starts with 1. Subsequent numbers are derived by describing the previous number in terms
 * of consecutive digits. E.g. 1 11 21 1211 111221 312211 13112221
 * Write a program that takes as input an integer n and returns the nth integer in the
 * look-and-say sequence. Return the result as a string.
 */

 // Problem 7.9
 /**
 * Write a program which takes as input a valid Roman number string s and returns the integer it corresponds to.
 * The Roman numeral representation of positive integers uses the symbols I,V, X, L, C,D,M. Each symbol represents a 
 * value, with I being 1, V being 5, X being 10, L being 50, C being 100, D being 500, and M being 1000.
 * A string over the Roman number symbols is a valid Roman number string if symbols appear in non-increasing order, 
 * with the following exceptions allowed:
 * • I can immediately precede V and X.
 * • X can immediately precede L and C.
 * • C can immediately precede D and M.
 */