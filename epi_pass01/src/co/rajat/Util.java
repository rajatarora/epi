package co.rajat;

import java.util.List;

public class Util {

    public static void printArray(int[] input) {
        for (int value: input) {
            System.out.print(value);
        }
        System.out.println();
    }

    public static void printList(List<Integer> input) {
        printArray(input.stream().mapToInt(Integer::intValue).toArray());
    }

}
