package co.rajat.epi;

class BinaryTreeNode {
    public int data;
    public BinaryTreeNode left, right, parent;

    public BinaryTreeNode(int data) {
        this.data = data;
    }

    public BinaryTreeNode(int data, BinaryTreeNode left, BinaryTreeNode right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public void set(BinaryTreeNode left, BinaryTreeNode right, BinaryTreeNode parent) {
        this.left = left;
        this.right = right;
        this.parent = parent;
    }
}

class BalanceStatus {
    public boolean balanced;
    public int height;

    public BalanceStatus(boolean balanced, int height) {
        this.balanced = balanced;
        this.height = height;
    }

    public static BalanceStatus checkBalance(BinaryTreeNode root) {
        if (root == null) return new BalanceStatus(true, -1);

        BalanceStatus left = checkBalance(root.left);
        if (!left.balanced) {
            return left;
        }

        BalanceStatus right = checkBalance(root.right);
        if (!right.balanced) {
            return right;
        }

        int height = Math.max(left.height, right.height) + 1;
        boolean balance = Math.abs(left.height - right.height) <= 1;
        return new BalanceStatus(balance, height);
    }
}

public class Problem10_1 {

    static <T> void inorder(BinaryTreeNode root) {
        if (root == null) return;
        inorder(root.left);
        System.out.print(root.data + " ");
        inorder(root.right);
    }

    static <T> void preorder(BinaryTreeNode root) {
        if (root == null) return;
        System.out.print(root.data + " ");
        preorder(root.left);
        preorder(root.right);
    }

    static <T> void postorder(BinaryTreeNode root) {
        if (root == null) return;
        postorder(root.left);
        postorder(root.right);
        System.out.print(root.data + " ");
    }

    public static void main(String[] args) {

        BinaryTreeNode balanced = buildBalancedTree();
        inorder(balanced);
        System.out.println();
        preorder(balanced);
        System.out.println();
        postorder(balanced);
        System.out.println();
        System.out.println(BalanceStatus.checkBalance(balanced).balanced);

        BinaryTreeNode unbalanced = buildUnbalancedTree();
        inorder(unbalanced);
        System.out.println();
        preorder(unbalanced);
        System.out.println();
        postorder(unbalanced);
        System.out.println();
        System.out.print(BalanceStatus.checkBalance(unbalanced).balanced);

    }

    static BinaryTreeNode buildBalancedTree() {
        BinaryTreeNode btn01 = new BinaryTreeNode(1);
        BinaryTreeNode btn02 = new BinaryTreeNode(2);
        BinaryTreeNode btn03 = new BinaryTreeNode(3);
        BinaryTreeNode btn04 = new BinaryTreeNode(4);
        BinaryTreeNode btn05 = new BinaryTreeNode(5);
        BinaryTreeNode btn06 = new BinaryTreeNode(6);
        BinaryTreeNode btn07 = new BinaryTreeNode(7);
        BinaryTreeNode btn08 = new BinaryTreeNode(8);
        BinaryTreeNode btn09 = new BinaryTreeNode(9);
        BinaryTreeNode btn10 = new BinaryTreeNode(0);
        BinaryTreeNode btn11 = new BinaryTreeNode(1);
        BinaryTreeNode btn12 = new BinaryTreeNode(2);
        BinaryTreeNode btn13 = new BinaryTreeNode(3);
        BinaryTreeNode btn14 = new BinaryTreeNode(4);
        BinaryTreeNode btn15 = new BinaryTreeNode(5);

        btn01.set(btn02, btn03, null);
        btn02.set(btn04, btn05, btn01);
        btn04.set(btn08, btn09, btn02);
        btn08.set(btn14, btn15, btn04);
        btn14.set(null, null, btn08);
        btn15.set(null, null, btn08);
        btn09.set(null, null, btn04);
        btn03.set(btn06, btn07, btn01);
        btn06.set(btn12, btn13, btn03);
        btn12.set(null, null, btn06);
        btn13.set(null, null, btn06);
        btn07.set(null, null, btn03);
        btn05.set(btn10, btn11, btn02);
        btn10.set(null, null, btn05);
        btn11.set(null, null, btn05);

        return btn01;
    }

    static BinaryTreeNode buildUnbalancedTree() {
        BinaryTreeNode btn01 = new BinaryTreeNode(1);
        BinaryTreeNode btn02 = new BinaryTreeNode(2);
        BinaryTreeNode btn03 = new BinaryTreeNode(3);
        BinaryTreeNode btn04 = new BinaryTreeNode(4);
        BinaryTreeNode btn05 = new BinaryTreeNode(5);
        BinaryTreeNode btn06 = new BinaryTreeNode(6);
        BinaryTreeNode btn07 = new BinaryTreeNode(7);
        BinaryTreeNode btn08 = new BinaryTreeNode(8);
        BinaryTreeNode btn09 = new BinaryTreeNode(9);

        btn01.set(btn02, btn03, null);
        btn02.set(btn04, btn05, btn01);
        btn03.set(null, null, btn01);
        btn04.set(btn06, btn07, btn02);
        btn05.set(null, null, btn02);
        btn06.set(btn08, btn09, btn04);
        btn07.set(null, null, btn04);
        btn08.set(null, null, btn06);
        btn09.set(null, null, btn06);

        return btn01;
    }
}
