package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

class ArrayEntry {
    public int value;
    public int arrayId;

    public ArrayEntry(int value, int arrayId) {
        this.value = value;
        this.arrayId = arrayId;
    }
}

public class Problem11_1 {
    public static void main(String[] args) {

        List<Integer> l1 = Arrays.asList(3,5,7);
        List<Integer> l2 = Arrays.asList(0,6);
        List<Integer> l3 = Arrays.asList(0,6,28);

        merge(Arrays.asList(l1,l2,l3));

    }

    static void merge(List<List<Integer>> data) {
        List<Integer> result = new ArrayList<>();
        List<Iterator<Integer>> iterators = new ArrayList<>();
        for (List<Integer> list : data) {
            iterators.add(list.iterator());
        }

        PriorityQueue<ArrayEntry> minHeap = new PriorityQueue<>(
                data.size(), Comparator.comparingInt(t0 -> t0.value)
        );

        for (int i = 0; i < iterators.size(); i++) {
            if (iterators.get(i).hasNext()) {
                minHeap.add(new ArrayEntry(iterators.get(i).next(), i));
            }
        }

        while (!minHeap.isEmpty()) {
            ArrayEntry head = minHeap.poll();
            result.add(head.value);
            if (iterators.get(head.arrayId).hasNext()) {
                minHeap.add(new ArrayEntry(iterators.get(head.arrayId).next(), head.arrayId));
            }
        }

        for (Integer x : result) {
            System.out.print(x + " ");
        }
    }
}
