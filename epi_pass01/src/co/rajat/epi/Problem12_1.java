package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Problem12_1 {

    public static void main(String[] args) {
        List<Integer> sample = Arrays.asList(-14,-10,2,108,243,285,285,285,401);
        System.out.println(binarySearch(sample, 285));
    }

    static int binarySearch(List<Integer> data, int key) {
        int left = 0, right = data.size() - 1, result = -1;
        while (left < right) {
            int mid = left + ((right - left) / 2);
            if (data.get(mid) < key) {
                left = mid + 1;
            }
            else if (data.get(mid) > key) {
                right = mid - 1;
            }
            else if (data.get(mid) == key) {
                result = mid;
                right = mid - 1;
            }
        }
        return result;
    }
}
