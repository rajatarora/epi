package co.rajat.epi;

import java.util.HashMap;
import java.util.Map;

/**
 * Write a program which takes text for an anonymous letter and text for a magazine
 * and determines if it is possible to write the anonymous letter using the magazine.
 * The anonymous letter can be written using the magazine if for each character in the
 * anonymous letter, the number of times it appears in the anonymous letter is no more
 * than the number of times it appears in the magazine.
 */
public class Problem13_2 {
    public static void main(String[] args) {
        String letter = "Hello World";
        String magazine1 = "Peter Says Hello. He Is A Good Citizen Of The World.";
        String magazine2 = "Peter Says Hello";
        System.out.println(isLetterPossible(letter, magazine1));
        System.out.println(isLetterPossible(letter, magazine2));
    }

    static boolean isLetterPossible(String letter, String magazine) {
        Map<Character, Integer> letterFrequency = new HashMap<>();
        for (int i = 0; i < letter.length(); i++) {
            Character c = letter.charAt(i);
            if (letterFrequency.containsKey(c)) {
                letterFrequency.put(c,
                        letterFrequency.get(c) + 1);
            }
            else {
                letterFrequency.put(c, 1);
            }
        }

        for (int i = 0; i < magazine.length(); i++) {
            Character c = magazine.charAt(i);
            if (letterFrequency.containsKey(c)) {
                letterFrequency.put(c,
                        letterFrequency.get(c) - 1);
                if (letterFrequency.get(c) == 0) {
                    letterFrequency.remove(c);
                }
                if (letterFrequency.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }
}
