package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a program which takes as input two sorted arrays, and returns a new array
 * containing elements that are present in both of the input arrays. The input arrays
 * may have duplicate entries, but the returned array should be free of duplicates.
 */
public class Problem14_1 {
    public static void main(String[] args) {
        List<Integer> a = Arrays.asList(2,2,3,3,5,5,6,7,7,8,12);
        List<Integer> b = Arrays.asList(5,5,6,8,8,9,10,10);
        System.out.println(intersect(a, b));
    }

    static List<Integer> intersect(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<>();

        for (int i = 0, j = 0, k = 0; i < a.size() && j < b.size();) {
            if (a.get(i).equals(b.get(j)) &&
                    (k == 0 || !result.get(k - 1).equals(a.get(i)))) {
                result.add(a.get(i));
                i++;
                j++;
                k++;
            }
            else {
                if (a.get(i) < b.get(j)) {
                    i++;
                } else {
                    j++;
                }
            }
        }
        return result;
    }
}
