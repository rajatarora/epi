package co.rajat.epi;

/**
 * Write a program that takes as input a binary tree and checks if the tree satisfies the
 * BST property.
 */

class BSTNode<T> {
    public T data;
    public BSTNode<T> left;
    public BSTNode<T> right;

    public BSTNode(T data) {
        this.data = data;
    }

    public void set(BSTNode<T> left, BSTNode<T> right) {
        this.left = left;
        this.right = right;
    }
}

public class Problem15_1 {
    public static void main(String[] args) {
        BSTNode<Integer> trueBST = buildTrueBST();
        BSTNode<Integer> falseBST = buildFalseBST();

        System.out.println(isBST(trueBST, Integer.MIN_VALUE, Integer.MAX_VALUE));
        System.out.println(isBST(falseBST, Integer.MIN_VALUE, Integer.MAX_VALUE));
    }

    static <T> boolean isBST(BSTNode<Integer> root, int min, int max) {
        if (root == null) {
            return true;
        }
        else if (root.data < min || root.data > max) {
            return false;
        }
        return (isBST(root.left, min, root.data) && isBST(root.right, root.data, max));
    }

    static BSTNode<Integer> buildTrueBST() {
        BSTNode<Integer> node01 = new BSTNode<>(25);
        BSTNode<Integer> node02 = new BSTNode<>(15);
        BSTNode<Integer> node03 = new BSTNode<>(35);
        BSTNode<Integer> node04 = new BSTNode<>(8);
        BSTNode<Integer> node05 = new BSTNode<>(18);
        BSTNode<Integer> node06 = new BSTNode<>(30);
        BSTNode<Integer> node07 = new BSTNode<>(40);
        BSTNode<Integer> node08 = new BSTNode<>(4);
        BSTNode<Integer> node09 = new BSTNode<>(12);
        BSTNode<Integer> node10 = new BSTNode<>(16);
        BSTNode<Integer> node11 = new BSTNode<>(22);
        BSTNode<Integer> node12 = new BSTNode<>(37);
        BSTNode<Integer> node13 = new BSTNode<>(42);

        node01.set(node02, node03);
        node02.set(node04, node05);
        node03.set(node06, node07);
        node04.set(node08, node09);
        node05.set(node10, node11);
        node07.set(node12, node13);

        return node01;
    }

    static BSTNode<Integer> buildFalseBST() {
        BSTNode<Integer> node01 = new BSTNode<>(25);
        BSTNode<Integer> node02 = new BSTNode<>(24);
        BSTNode<Integer> node03 = new BSTNode<>(38);
        BSTNode<Integer> node04 = new BSTNode<>(17);
        BSTNode<Integer> node05 = new BSTNode<>(27);
        BSTNode<Integer> node06 = new BSTNode<>(36);
        BSTNode<Integer> node07 = new BSTNode<>(42);
        BSTNode<Integer> node08 = new BSTNode<>(22);
        BSTNode<Integer> node09 = new BSTNode<>(40);

        node01.set(node02, node03);
        node02.set(node04, node05);
        node03.set(node06, node07);
        node06.set(node08, node09);

        return node01;
    }
}
