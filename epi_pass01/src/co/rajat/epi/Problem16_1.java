package co.rajat.epi;

/**
 * Tower of Hanoi problem
 * Write a program which prints a sequence of operations that transfers n rings from one
 * peg to another.
 */
public class Problem16_1 {
    public static void main(String[] args) {
        towerOfHanoi(6, 'A', 'B', 'C');
    }

    static void towerOfHanoi(int rings, char from, char to, char intermediary) {
        if (rings == 1) {
            System.out.println("Ring 1: " + from + " to " + to);
            return;
        }
        towerOfHanoi(rings - 1, from, intermediary, to);
        System.out.println("Ring " + rings + ": " + from + " to " + to);
        towerOfHanoi(rings - 1, intermediary, to, from);
    }
}
