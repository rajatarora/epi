package co.rajat.epi;

import co.rajat.Util;

/**
 * Your input is an array of integers, and you have to reorder its entries so that the even entries appear first. This
 * is easy if you use 0(n) space, where n is the length of the array. However, you are required to solve it without
 * allocating additional storage.
 */
public class Problem6_0 {

    public static void main(String[] args) {
        int[] test1 = {1,2,3,4,5,6,7,8,9};
        int[] test2 = {9,8,7,6,5,4,3,2,1};
        int[] test3 = {1};
        int[] test4 = {2};
        int[] test5 = {};
        int[] test6 = {1,3,5,7,9};
        int[] test7 = {2,4,6,8};

        run(test1);
        run(test2);
        run(test3);
        run(test4);
        run(test5);
        run(test6);
        run(test7);
    }

    static int[] evenFirst(int[] input) {
        /*
        Basic approach is: start from beginning. If a number is even, keep it where it is. If a number is odd, send it
        to the back of the array.
         */
        int even = 0, odd = input.length - 1;
        while (even < odd) {
            if (input[even] %2 == 0) {
                even++;
            }
            else {
                int tmp = input[even];
                input[even] = input[odd];
                input[odd] = tmp;
                odd--;
            }
        }
        return input;
    }

    static void run(int[] input) {
        System.out.print("Input: ");
        Util.printArray(input);
        System.out.print("Output: ");
        Util.printArray(evenFirst(input));
    }

}
