package co.rajat.epi;

import co.rajat.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * The Dutch National Flag problem
 * Write a program that takes an array A and an index i into A, and rearranges the
 * elements such that all elements less than A[i] (the "pivot") appear first, followed by
 * elements equal to the pivot, followed by elements greater than the pivot.
 */
public class Problem6_1 {

    public static void main(String[] args) {
        ArrayList<Integer> test1 = new ArrayList<>(Arrays.asList(9,8,7,4,4,5,4,2,2,1));
        ArrayList<Integer> test2 = new ArrayList<>(Arrays.asList(4,4,4,2,2,1));
        ArrayList<Integer> test3 = new ArrayList<>(Arrays.asList(9,8,7,4,4,5,4));
        ArrayList<Integer> test4 = new ArrayList<>(Arrays.asList(4,4,4));
        ArrayList<Integer> test5 = new ArrayList<>(Arrays.asList(9,9));
        ArrayList<Integer> test6 = new ArrayList<>(Arrays.asList(1,1));

        run(test1, 4);
        run(test2, 4);
        run(test3, 4);
        run(test4, 4);
        run(test5, 4);
        run(test6, 4);
    }

    public static ArrayList<Integer> dutchNationalFlag(ArrayList<Integer> input, int pivot) {
        /*
        Basic approach: The problem is to be solved without using any additional space. The input is an array list of
        numbers, and a pivot (which is to be in the middle). We'll maintain three indices... lower (0), pivot(0),
        and greater (size - 1).
        Start from beginning. If pivot, let it stay. If higher, send it to back. If lower, send it back?
         */

        int lower = 0, middle = 0, higher = input.size() - 1;
        while (middle <= higher) {
            if (input.get(middle) < pivot) {
                Collections.swap(input, lower++, middle++);
            }
            else if (input.get(middle) == pivot) {
                middle++;
            }
            else {
                Collections.swap(input, middle, higher--);
            }
        }

        return input;
    }

    static void run(ArrayList<Integer> input, int pivot) {
        System.out.print("Input: ");
        Util.printList(input);
        System.out.print("Output: ");
        Util.printList(dutchNationalFlag(input, pivot));
    }
}
