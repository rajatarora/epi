package co.rajat.epi;

import co.rajat.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Write a program which takes as input an array of digits encoding a decimal number
 * D and a integer n and updates the array to represent the number D + n
 */
public class Problem6_2 {
    public static void main(String[] args) {
        List<Integer> test1 = Arrays.asList(1,2,3);
        List<Integer> test2 = Arrays.asList(9,9,9);

        run(test1, 1);
        run(test2, 12345);
    }

    public static List<Integer> add(List<Integer> input, int number) {
        List<Integer> result = new ArrayList<>(Collections.nCopies(input.size() + 1, 0));
        int carry = number;
        for (int i = input.size() - 1; i >= 0; i--) {
            result.set(i + 1, input.get(i) + carry);
            carry = 0;
            if (result.get(i + 1) > 9) {
                int tmp = result.get(i + 1);
                result.set(i + 1, tmp % 10);
                carry = tmp / 10;
            }
        }
        if (carry != 0) {
            result.set(0, carry);
        }
        if (result.get(0) == 0) {
            result.remove(0);
        }
        return result;
    }

    static void run(List<Integer> input, int number) {
        System.out.print("Input: ");
        Util.printList(input);
        System.out.print("Output: ");
        Util.printList(add(input, number));
    }
}
