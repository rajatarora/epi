package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a program which takes as input an array of digits encoding a decimal number
 * D and updates the array to represent the number D + 1
 */
public class Problem6_2_0 {
    public static void main(String[] args) {
        List<Integer> test1 = new ArrayList<>(Arrays.asList(1,2,3));
        List<Integer> test2 = new ArrayList<>(Arrays.asList(9,9,9));

        System.out.println(increment(test1));
        System.out.println(increment(test2));
    }

    static List<Integer> increment(List<Integer> number) {
        int n = number.size() - 1;
        number.set(n, number.get(n) + 1);
        for (int i = n; i > 0 && number.get(i) == 10; i--) {
            number.set(i, 0);
            number.set(i - 1, number.get(i - 1) + 1);
        }
        if (number.get(0) == 10) {
            number.set(0, 0);
            number.add(0, 1);
        }
        return number;
    }
}
