package co.rajat.epi;

import co.rajat.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Write a program which takes as input two strings s and t of bits encoding
 * binary numbers Bs and Bt, respectively, and returns a new string of bits representing
 * the number Bs + Bt.
 */
public class Problem6_2_1 {
    public static void main(String[] args) {
        List<Integer> test1a = Arrays.asList(1,1,1,0,0,0);
        List<Integer> test1b = Arrays.asList(1,1,1);

        List<Integer> test2a = Collections.singletonList(1);

        List<Integer> test3a = Arrays.asList(1,1,1);
        List<Integer> test3b = Arrays.asList(1,1,0);

        run(test1a, test1b);
        run(test2a, test2a);
        run(test3a, test3b);
    }

    static List<Integer> binaryAdder(List<Integer> a, List<Integer> b) {
        int maxLen = Math.max(a.size(), b.size());
        List<Integer> smallerInput = a.size() < b.size() ? a : b;
        List<Integer> biggerInput = a.size() >= b.size() ? a : b;
        smallerInput = leftPad(smallerInput, biggerInput.size() - smallerInput.size());
        List<Integer> result = new ArrayList<>(Collections.nCopies(maxLen + 1, 0));

        int carry = 0;
        for (int i = biggerInput.size() - 1; i >= 0; i--) {
            int digit = smallerInput.get(i) + biggerInput.get(i) + carry;
            switch(digit) {
                case 0:
                case 1: result.set(i + 1, digit); carry = 0; break;
                case 2: result.set(i + 1, 0); carry = 1; break;
                case 3: result.set(i + 1, 1); carry = 1; break;
            }
        }

        if (carry != 0) {
            result.set(0, carry);
        }
        if (result.get(0) == 0) {
            result.remove(0);
        }
        return result;
    }

    static List<Integer> leftPad(List<Integer> input, int num) {
        List<Integer> result = new ArrayList<>(Collections.nCopies(input.size() + num, 0));
        for (int i = 0; i < input.size(); i++) {
            result.set(num + i, input.get(i));
        }
        return result;
    }

    static void run(List<Integer> op1, List<Integer> op2) {
        System.out.println(op1 + " + " + op2 + " = " + binaryAdder(op1, op2));
    }
}
