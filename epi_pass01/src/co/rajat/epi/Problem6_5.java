package co.rajat.epi;

import co.rajat.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a program which takes as input a sorted array and updates it so that all duplicates have been removed and the
 * remaining elements have been shifted left to fill the emptied indices. Return the number of valid elements.
 */
public class Problem6_5 {
    public static void main(String[] args) {
        ArrayList<Integer> test1 = new ArrayList<>(Arrays.asList(1,2,2,3,4,5,5,6,6,6,7,9));
        run(test1);
    }

    static List<Integer> removeDuplicates(List<Integer> input) {
        int writeCursor = 1;
        for (int i = 1; i < input.size(); i++) {
            if (!input.get(i).equals(input.get(writeCursor - 1))) {
                input.set(writeCursor++, input.get(i));
            }
        }
        for (int i = writeCursor; i < input.size(); i++) {
            input.set(i, 0);
        }
        return input;
    }

    static void run(List<Integer> input) {
        System.out.println("Input: " + input);
        System.out.println("Output: " + removeDuplicates(input));
    }
}
