package co.rajat.epi;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement a function which takes as input an array and a key and updates
 * the array so that all occurrences of the input key have been removed and the remaining
 * elements have been shifted left to fill the emptied indices. Return the number of
 * remaining elements. There are no requirements as to the values stored beyond the
 * last valid element.
 */
public class Problem6_5_1 {
    public static void main(String[] args) {
        ArrayList<Integer> test1 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,7,7,7,8,9));
        ArrayList<Integer> test2 = new ArrayList<>(Arrays.asList(7,7));
        ArrayList<Integer> test3 = new ArrayList<>(Arrays.asList(1,7,7,7,7,7,7,7));
        ArrayList<Integer> test4 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,8,9));
        run(test1, 7);
        run(test2, 7);
        run(test3, 7);
        run(test4, 7);
    }

    static List<Integer> removeKey(List<Integer> input, int key) {
        int writeCursor = 0;
        for (int i = 0; i < input.size(); i++) {
            if (!input.get(i).equals(key)) {
                input.set(writeCursor++, input.get(i));
            }
        }
        for (int i = writeCursor; i < input.size(); i++) {
            input.set(i, 0);
        }
        return input;
    }

    static void run(List<Integer> input, int key) {
        System.out.println("Input: " + input);
        System.out.println("Output: " + removeKey(input, key));
    }
}
