package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Write a program which takes as input a sorted array A of integers and a
 * positive integer m, and updates A so that if x appears m times in A it appears exactly
 * min(2,m) times in A. The update to A should be performed in one pass, and no
 * additional storage may be allocated.
 */
public class Problem6_5_2 {
    public static void main(String[] args) {
        ArrayList<Integer> test1 = new ArrayList<>(Arrays.asList(1,2,2,2,2,3,3,3,4));
        run(test1, 2);
    }

    static ArrayList<Integer> trimList(ArrayList<Integer> input, int m) {
        int currentNum = -99999999;
        int threshold = 0;
        int writeCursor = 0;
        for (int i = 0; i < input.size(); i++) {
            if (currentNum != input.get(i)) {
                currentNum = input.get(i);
                threshold = Math.min(2, m) - 1;
                writeCursor = i;
                continue;
            }
            if (threshold > 0 && input.get(i).equals(currentNum)) {
                threshold--;
                writeCursor++;
            }
            else if (threshold <= 1 && !input.get(i).equals(currentNum)) {
                input.set(writeCursor++, input.get(i));
            }
        }
        return input;
    }

    static void run(ArrayList<Integer> input, int m) {
        System.out.println("Input: " + input);
        System.out.println("Output: " + trimList(input, m));
    }
}
