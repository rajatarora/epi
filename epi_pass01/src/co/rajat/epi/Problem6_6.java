package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a program that takes an array denoting the daily stock price, and returns the
 * maximum profit that could be made by buying and then selling one share of that
 * stock.
 */
public class Problem6_6 {
    public static void main(String[] args) {
        List<Integer> test1 = Arrays.asList(310, 315, 275, 295, 260, 270, 290, 230, 255, 250);
        System.out.println(stocks(test1));

    }

    static int stocks(List<Integer> prices) {
        int maxProfit = 0, min = 99999999;
        for (Integer price : prices) {
            if (min > price) {
                min = price;
            }
            if (maxProfit < price - min) {
                maxProfit = price - min;
            }
        }
        return maxProfit;
    }
}
