package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a program which takes an nxn 2D array and returns the spiral ordering of the
 * array.
 */
public class Problem6_7 {

    public static void main(String[] args) {
        List<Integer> r1 = Arrays.asList(1,2,3);
        List<Integer> r2 = Arrays.asList(4,5,6);
        List<Integer> r3 = Arrays.asList(7,8,9);
        List<List<Integer>> test = Arrays.asList(r1,r2,r3);
        System.out.println(spiralOrder(test));
    }

    static List<Integer> spiralOrder(List<List<Integer>> matrix) {
        int[][] direction = {{0,1},{1,0},{0,-1},{-1,0}};
        int dir = 0, x = 0, y = 0, n = matrix.size();
        List<Integer> result = new ArrayList<>();
        for(int i = 0; i < n * n; i++) {
            result.add(matrix.get(x).get(y));
            matrix.get(x).set(y, Integer.MIN_VALUE);
            int nextX = x + direction[dir][0];
            int nextY = y + direction[dir][1];
            if (nextX >= n ||
                    nextX < 0 ||
                    nextY >= n ||
                    nextY < 0 ||
                    matrix.get(nextX).get(nextY) == Integer.MIN_VALUE) {
                dir = (dir + 1) % 4;
                nextX = x + direction[dir][0];
                nextY = y + direction[dir][1];
            }
            x = nextX;
            y = nextY;
        }
        return result;
    }

}
