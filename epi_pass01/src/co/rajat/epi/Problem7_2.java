package co.rajat.epi;

/**
 * Write a program that performs base conversion. The input is a string, an integer b2 , and
 * another integer b2. The string represents be an integer in base b2. The output should
 * be the string representing the integer in base b2.
 */
public class Problem7_2 {

    public static void main(String[] args) {
        System.out.println(convertBase("615", 7, 13));
    }

    static String convertBase(String number, int fromBase, int toBase) {
        int decimal = toDecimal(number, fromBase);
        System.out.println(decimal);
        return toBase(decimal, toBase);
    }

    static int toDecimal(String number, int base) {
        int result = 0, power = 0;
        for (int i = number.length() - 1; i >= 0; i--) {
            char c = number.charAt(i);
            if (Character.isDigit(c)) {
                result += ((int) c - '0') * Math.pow(base, power++);
            }
            else if (Character.isAlphabetic(c)) {
                c = Character.toUpperCase(c);
                result += ((int) c - 'A' + 10) * Math.pow(base, power++);
            }
            else {
                throw new IllegalArgumentException("String cannot be represented as a number");
            }
        }
        return result;
    }

    static String toBase(int number, int base) {
        StringBuilder result = new StringBuilder();

        while (number > 0) {
            int digit = number % base;
            number = number / base;
            if (digit < 10) {
                result.append(digit);
            }
            else {
                result.append((char)('A' + (digit - 10)));
            }
        }
        return result.reverse().toString();
    }
}
