package co.rajat.epi;

/**
 * Implement a function that converts a spreadsheet column id to the corresponding
 * integer, with "A" corresponding to 1. For example, you should return 4 for "D", 27
 * for "AA", 702 for "ZZ", etc.
 */
public class Problem7_3 {
    public static void main(String[] args) {
        System.out.println(decode("A"));
        System.out.println(decode("AB"));
        System.out.println(decode("ZZ"));
        System.out.println(decode("DR"));
        System.out.println(decode("DDD"));
    }

    static int decode(String columnId) {
        int result = 0;
        for (int i = columnId.length() - 1; i >= 0; i--) {
            result = result * 26 + (columnId.charAt(i) - 'A' + 1);
        }
        return result;
    }
}
