package co.rajat.epi;

import java.util.ArrayList;
import java.util.List;

/**
 * Implement a function that converts an integer to the spreadsheet column id. For example, you
 * should return "D" for 4, "AA" for 27, and "ZZ" for 702
 */
public class Problem7_3_1 {
    public static void main(String[] args) {
        System.out.println(encode(1));
        System.out.println(encode(27));
        System.out.println(encode(702));
        System.out.println(encode(9982));
    }

    static List<Character> encode(int column) {
        ArrayList<Character> result = new ArrayList<>();
        while (column > 0) {
            int digit = column % 26;
            if (digit == 0) {
                result.add(0, 'Z');
                column = (column / 26) - 1;
            }
            else {
                result.add(0, (char) ((digit - 1) + 'A'));
                column /= 26;
            }
        }
        return result;
    }
}
