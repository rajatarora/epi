package co.rajat.epi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Write a program which takes as input a phone number, specified as a string of digits,
 * and returns all possible character sequences that correspond to the phone number.
 * The cell phone keypad is specified by a mapping that takes a digit and returns the
 * corresponding set of characters. The character sequences do not have to be legal
 * words or phrases.
 */
public class Problem7_7 {

    static List<String> phone = Arrays.asList("", "", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ");
    static List<String> combinations = new ArrayList<>();

    public static void main(String[] args) {
        findCombo("", "789");
        System.out.println(combinations);
    }

    static void findCombo(String partialCombo, String remainingNumber) {
        if (remainingNumber.length() == 0) {
            combinations.add(partialCombo);
        }
        else {
            int digit = remainingNumber.charAt(0) - '0';
            String letters = phone.get(digit);
            for (int i = 0; i < letters.length(); i++) {
                char letter = letters.charAt(i);
                findCombo(partialCombo + letter, remainingNumber.substring(1));
            }
        }
    }
}
