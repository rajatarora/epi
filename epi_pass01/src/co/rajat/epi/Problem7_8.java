package co.rajat.epi;

/**
 * The look-and-say sequence starts with 1. Subsequent numbers are derived by describing the previous number in terms
 * of consecutive digits. E.g. 1 11 21 1211 111221 312211 13112221
 * Write a program that takes as input an integer n and returns the nth integer in the
 * look-and-say sequence. Return the result as a string.
 */
public class Problem7_8 {
    public static void main(String[] args) {
        System.out.println(lookAndSay(1));
        System.out.println(lookAndSay(2));
        System.out.println(lookAndSay(3));
        System.out.println(lookAndSay(4));
        System.out.println(lookAndSay(5));
        System.out.println(lookAndSay(6));
        System.out.println(lookAndSay(7));
        System.out.println(lookAndSay(8));
        System.out.println(lookAndSay(9));
        System.out.println(lookAndSay(10));

    }

    static String lookAndSay(int n) {
        String lookAndSay = "1";
        for (int i = 1; i < n; i++) {
            lookAndSay = buildLookAndSay(lookAndSay);
        }
        return lookAndSay;
    }

    static String buildLookAndSay(String input) {
        int counter;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            counter = 1;
            while (i + 1 < input.length() && input.charAt(i) == input.charAt(i + 1)) {
                i++;
                counter++;
            }
            result.append(counter);
            result.append(input.charAt(i));
        }
        return result.toString();
    }

}
