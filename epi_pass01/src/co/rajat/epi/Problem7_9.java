package co.rajat.epi;

/**
 * Write a program which takes as input a valid Roman number string s and returns the integer it corresponds to.
 * The Roman numeral representation of positive integers uses the symbols I,V, X, L, C,D,M. Each symbol represents a
 * value, with I being 1, V being 5, X being 10, L being 50, C being 100, D being 500, and M being 1000.
 * A string over the Roman number symbols is a valid Roman number string if symbols appear in non-increasing order,
 * with the following exceptions allowed:
 * • I can immediately precede V and X.
 * • X can immediately precede L and C.
 * • C can immediately precede D and M.
 */
public class Problem7_9 {
}
