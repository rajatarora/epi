package co.rajat.epi;

/**
 * Consider a singly linked list whose nodes are numbered starting at 0. Define the even-
 * odd merge of the list to be the list consisting of the even-numbered nodes followed
 * by the odd-numbered nodes.
 */
public class Problem8_10 {
    public static void main(String[] args) {

        ListNode a5 = new ListNode(5, null);
        ListNode a4 = new ListNode(4, a5);
        ListNode a3 = new ListNode(3, a4);
        ListNode a2 = new ListNode(2, a3);
        ListNode a1 = new ListNode(1, a2);
        ListNode a0 = new ListNode(0, a1);
        print(a0);
        print(evenOddMerge(a0));

    }

    static ListNode evenOddMerge(ListNode start) {
        int index = 0;
        ListNode even = null, odd = null, head = start;
        ListNode evenTail = null, oddTail = null;
        while (head != null) {
            if (index % 2 == 0) {
                if (even == null) {
                    even = head;
                    evenTail = head;
                }
                else {
                    evenTail.next = head;
                    evenTail = evenTail.next;
                }
            }
            else {
                if (odd == null) {
                    odd = head;
                    oddTail = head;
                }
                else {
                    oddTail.next = head;
                    oddTail = oddTail.next;
                }
            }
            index++;
            head = head.next;
        }
        oddTail.next = null;
        evenTail.next = odd;
        return even;
    }

    static void print(ListNode start) {
        while (start != null) {
            System.out.print(start.data);
            if (start.next != null) {
                System.out.print(" -> ");
            }
            start = start.next;
        }
        System.out.println();
    }
}
