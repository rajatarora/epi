package co.rajat.epi;

/**
 * Write a program that tests whether a singly linked list is palindromic.
 */
public class Problem8_11 {
    public static void main(String[] args) {

        ListNode a5 = new ListNode(0, null);
        ListNode a4 = new ListNode(1, a5);
        ListNode a3 = new ListNode(2, a4);
        ListNode a2 = new ListNode(1, a3);
        ListNode a1 = new ListNode(0, a2);
        ListNode a0 = new ListNode(0, a1);

//        System.out.println(isPalindrome(a0));
        System.out.println(isPalindrome(a1));

    }

    static boolean isPalindrome(ListNode start) {
        ListNode fast = start, slow = start;
        ListNode middle = null, middlePrev = null;

        while (fast != null && fast.next != null) {
            middlePrev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }

        // Odd number of elements
        if (fast != null) {
            middle = slow;
            slow = slow.next;
        }

        // At this point, first half = 0 to middlePrev
        // second half = slow to end
        ListNode reverseSecond = reverse(slow);
        return compare(start, reverseSecond);
    }

    static ListNode reverse(ListNode start) {
        ListNode prev = null, curr = start, third = null;
        while (curr != null) {
            third = curr.next;
            curr.next = prev;
            prev = curr;
            curr = third;
        }
        start = prev;
        return start;
    }

    static boolean compare(ListNode one, ListNode two) {
        while (one != null && two != null) {
            if(one.data != two.data) {
                return false;
            }
            one = one.next;
            two = two.next;
        }
        return true;
    }
}
