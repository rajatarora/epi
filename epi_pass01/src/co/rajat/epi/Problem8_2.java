package co.rajat.epi;

/**
 * Write a program which takes a singly linked list L and two integers s and / as
 * arguments, and reverses the order of the nodes from the sth node to /th node,
 * inclusive. The numbering begins at 1, i.e., the head node is the first node. Do not
 * allocate additional nodes.
 */

class ListNode {
    int data;
    ListNode next;

    public ListNode(int data) {
        this.data = data;
    }

    public ListNode(int data, ListNode next) {
        this.data = data;
        this.next = next;
    }

    public String toString() {
        return "Data: " + this.data;
    }
}

public class Problem8_2 {
    public static void main(String[] args) {
        ListNode a6 = new ListNode(10, null);
        ListNode a5 = new ListNode(2, a6);
        ListNode a4 = new ListNode(7, a5);
        ListNode a3 = new ListNode(5, a4);
        ListNode a2 = new ListNode(3, a3);
        ListNode a1 = new ListNode(11, a2);
        print(a1);
        reverse(a1, 2, 5);

    }

    static void reverse(ListNode start, int s, int f) {
        ListNode prev = null, curr = start;
        while (s > 1) {
            prev = curr;
            curr = curr.next;
            s--;
            f--;
        }
        ListNode con = prev, tail = curr, third = null;
        while (f > 0) {
            third = curr.next;
            curr.next = prev;
            prev = curr;
            curr = third;
            f--;
        }
        if (con != null) {
            con.next = prev;
        }
        else {
            start = prev;
        }
        tail.next = curr;
        print(start);
    }

    static void print(ListNode start) {
        while (start != null) {
            System.out.print(start.data);
            if (start.next != null) {
                System.out.print(" -> ");
            }
            start = start.next;
        }
        System.out.println();
    }
}
