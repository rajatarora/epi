package co.rajat.epi;

/**
 * Write a program that takes the head of a singly linked list and returns null if there
 * does not exist a cycle, and the node at the start of the cycle, if a cycle is present. (You
 * do not know the length of the list in advance.)
 */

public class Problem8_3 {
    public static void main(String[] args) {
        ListNode start = buildCyclicList();
        System.out.println(findCycle(start));
    }

    static ListNode findCycle(ListNode start) {
        ListNode fast = start, slow = start;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (slow.equals(fast)) {
                // There's a cycle! Find its head.
                slow = start;
                while (!slow.equals(fast)) {
                    slow = slow.next;
                    fast = fast.next;
                }
                return slow;
            }
        }
        return null;
    }

    static ListNode buildCyclicList() {
        ListNode a9 = new ListNode(9, null);
        ListNode a8 = new ListNode(8, a9);
        ListNode a7 = new ListNode(7, a8);
        ListNode a6 = new ListNode(6, a7);
        ListNode a5 = new ListNode(5, a6);
        ListNode a4 = new ListNode(4, a5);
        ListNode a3 = new ListNode(3, a4);
        ListNode a2 = new ListNode(2, a3);
        ListNode a1 = new ListNode(1, a2);
        a9.next = a4;
        return a1;
    }
}
