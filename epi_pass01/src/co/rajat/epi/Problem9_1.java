package co.rajat.epi;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Design a stack that includes a max operation, in addition to push and pop. The max
 * method should return the maximum value stored in the stack.
 */

class CachedMaxNode {
    public Integer data;
    public Integer max;

    public CachedMaxNode(int data, int max) {
        this.data = data;
        this.max = max;
    }
}

class CachedMaxStack {
    private final Deque<CachedMaxNode> stack = new LinkedList<>();

    public boolean isEmpty() {
        return this.stack.isEmpty();
    }

    public Integer max() {
        return this.stack.peek() != null ? this.stack.peek().max : null;
    }

    public Integer pop() {
        return this.isEmpty() ? null : this.stack.pop().data;
    }

    public void push(Integer data) {
        int max = Math.max(data, isEmpty() ? data : max());
        this.stack.push(new CachedMaxNode(data, max));
    }

    public void printState() {
        System.out.println("Max: " + max());
    }
}

class CountNode {
    public Integer max;
    public Integer frequency;

    public CountNode(Integer max, Integer frequency) {
        this.max = max;
        this.frequency = frequency;
    }

    public void increment() {
        this.frequency++;
    }

    public void decrement() {
        this.frequency--;
    }
}

class CountStack {
    private Deque<CountNode> countStack = new LinkedList<>();
    private Deque<Integer> stack = new LinkedList<>();

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    public void push(Integer data) {
        stack.push(data);
        if (countStack.isEmpty() || countStack.peek().max < data) {
            countStack.push(new CountNode(data, 1));
        }
        else if (countStack.peek().max.equals(data)) {
            countStack.peek().increment();
        }
    }

    public Integer pop() {
        if (stack.isEmpty()) {
            throw new IllegalStateException();
        }
        Integer data = stack.pop();
        if (countStack.isEmpty()) throw new IllegalStateException();
        if (countStack.peek().max.equals(data)) {
            countStack.peek().decrement();
            if (countStack.peek().frequency == 0) {
                countStack.pop();
            }
        }
        return data;
    }

    public Integer max() {
        if (countStack.isEmpty()) throw new IllegalStateException();
        return countStack.peek().max;
    }
}


public class Problem9_1 {
    public static void main(String[] args) {
        CachedMaxStack cachedMaxStack = new CachedMaxStack();
        cachedMaxStack.push(5); cachedMaxStack.printState();
        cachedMaxStack.push(8); cachedMaxStack.printState();
        cachedMaxStack.push(9); cachedMaxStack.printState();
        cachedMaxStack.push(5); cachedMaxStack.printState();
        cachedMaxStack.push(9); cachedMaxStack.printState();
        System.out.println(cachedMaxStack.pop()); cachedMaxStack.printState();
        System.out.println(cachedMaxStack.pop()); cachedMaxStack.printState();
        System.out.println(cachedMaxStack.pop()); cachedMaxStack.printState();
        System.out.println(cachedMaxStack.pop()); cachedMaxStack.printState();
        System.out.println(cachedMaxStack.pop()); cachedMaxStack.printState();
    }
}
