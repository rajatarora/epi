package co.rajat.epi;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Write a program that tests if a string made up of the characters '(', ')', '[',']', and '{','}'
 * is well-formed.
 */
public class Problem9_3 {
    public static void main(String[] args) {
        String a = "()[]{}";
        String b = "(hello world)";
        String c = "{hello world]";
        String d = "{[}]";
        String e = "[(hello){world}]";
        System.out.println(isWellFormed(a));
        System.out.println(isWellFormed(b));
        System.out.println(isWellFormed(c));
        System.out.println(isWellFormed(d));
        System.out.println(isWellFormed(e));
    }

    static boolean isWellFormed(String input) {
        Deque<Character> lefts = new LinkedList<>();
        for (int i = 0; i < input.length(); i++) {
            char push = input.charAt(i);
            if (push == '(' || push == '[' || push == '{') {
                lefts.push(push);
            }
            else if (push == ')' || push == ']' || push == '}') {
                if (lefts.isEmpty()) return false;
                char peek = lefts.peek();
                if ((push == ')' && peek != '(') ||
                        (push == ']' && peek != '[') ||
                        (push == '}' && peek != '{')) return false;
                lefts.pop();
            }
        }
        return lefts.isEmpty();
    }
}
