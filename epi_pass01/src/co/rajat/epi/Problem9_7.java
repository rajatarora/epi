package co.rajat.epi;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Given a binary tree, return an array consisting of the keys at the same level. Keys
 * should appear in the order of the corresponding nodes' depths, breaking ties from left
 * to right.
 * AKA: Level order traversal of a Binary Tree
 */
public class Problem9_7 {
    public static void main(String[] args) {
        BinaryTreeNode root = buildTree();
        System.out.println(levelOrder(root));
    }

    static List<List<Integer>> levelOrder(BinaryTreeNode root) {
        Deque<BinaryTreeNode> currentDepth = new LinkedList<>();
        List<List<Integer>> result = new ArrayList<>();
        currentDepth.add(root);
        while (!currentDepth.isEmpty()) {
            Deque<BinaryTreeNode> nextDepth = new LinkedList<>();
            List<Integer> elements = new ArrayList<>();
            while (!currentDepth.isEmpty()) {
                BinaryTreeNode node = currentDepth.poll();
                if (node != null) {
                    elements.add(node.data);
                    nextDepth.add(node.left);
                    nextDepth.add(node.right);
                }
            }
            if (!elements.isEmpty()) {
                result.add(elements);
            }
            currentDepth = nextDepth;
        }
        return result;
    }

    static BinaryTreeNode buildTree() {
        BinaryTreeNode n16 = new BinaryTreeNode(641, null, null);
        BinaryTreeNode n15 = new BinaryTreeNode(257, null, null);
        BinaryTreeNode n14 = new BinaryTreeNode(401, null, n16);
        BinaryTreeNode n13 = new BinaryTreeNode(17, null, null);
        BinaryTreeNode n12 = new BinaryTreeNode(28, null, null);
        BinaryTreeNode n11 = new BinaryTreeNode(1, n14, n15);
        BinaryTreeNode n10 = new BinaryTreeNode(3, n13, null);
        BinaryTreeNode n09 = new BinaryTreeNode(0, null, null);
        BinaryTreeNode n08 = new BinaryTreeNode(28, null, null);
        BinaryTreeNode n07 = new BinaryTreeNode(271, null, n12);
        BinaryTreeNode n06 = new BinaryTreeNode(2, null, n11);
        BinaryTreeNode n05 = new BinaryTreeNode(561, null, n10);
        BinaryTreeNode n04 = new BinaryTreeNode(271, n08, n09);
        BinaryTreeNode n03 = new BinaryTreeNode(6, n06, n07);
        BinaryTreeNode n02 = new BinaryTreeNode(6, n04, n05);
        BinaryTreeNode n01 = new BinaryTreeNode(314, n02, n03);
        return n01;
    }
}
