package co.rajat.epi;

import java.util.Arrays;
import java.util.List;

/**
 * Write a program that takes an array denoting the daily stock price, and returns the
 * maximum profit that could be made by buying and then selling one share of that
 * stock.
 */
public class Problem6_6 {
    public static void main(String[] args) {
        List<Integer> test1 = Arrays.asList(310, 315, 275, 295, 260, 270, 290, 230, 255, 250);
        System.out.println(maxProfit(test1));
    }

    /**
     * Maintain two variables, the min price (seen so far) and the max profit (seen so far). Max profit can only be made
     * by selling the stock at current price, when bought at the minimum price seen so far.
     * @param prices
     * @return max profit
     */
    static int maxProfit(List<Integer> prices) {
        int maxProfit = Integer.MIN_VALUE;
        int minPrice = Integer.MAX_VALUE;

        for (Integer price : prices) {
            minPrice = Math.min(minPrice, price);
            maxProfit = Math.max(maxProfit, (price - minPrice));
        }

        return maxProfit;
    }
}
