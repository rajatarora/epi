package co.rajat.epi;

/**
 * In this problem, you are to implement methods that take a string
 * representing an integer and return the corresponding integer, and vice versa. Your
 * code should handle negative integers. You cannot use library functions like stoi in
 * C++ and parselnt in Java.
 */
public class Problem7_1 {
    public static void main(String[] args) {
        System.out.println(itoa(-25871));
        System.out.println(atoi("-25871"));
    }

    static int atoi(String number) {
        boolean negative = false;
        if (number.charAt(0) == '-') {
            negative = true;
            number = number.substring(1);
        }
        int result = 0;
        for (int i = number.length() - 1, power = 0; i >= 0; i--, power++) {
            char c = number.charAt(i);
            if (Character.isDigit(c)) {
                result += ((c - '0') * Math.pow(10, power));
            }
        }
        if (negative) {
            result *= -1;
        }
        return result;
    }

    static String itoa(int number) {
        int abs = Math.abs(number);
        StringBuilder builder = new StringBuilder();
        while (abs > 0) {
            builder.append(abs % 10);
            abs /= 10;
        }
        if (number < 0) {
            builder.append("-");
        }
        return builder.reverse().toString();
    }
}
