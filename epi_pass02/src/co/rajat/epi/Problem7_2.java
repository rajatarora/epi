package co.rajat.epi;

/**
 * Write a program that performs base conversion. The input is a string, an integer b1 , and
 * another integer b2. The string represents be an integer in base b1. The output should
 * be the string representing the integer in base b2.
 */
public class Problem7_2 {
    public static void main(String[] args) {
        System.out.println(convertBase("615", 7, 13));
    }

    static String convertBase(String number, int fromBase, int toBase) {
        int decimal = toDecimal(number, fromBase);
        System.out.println(decimal);
        return toBase(decimal, toBase);
    }

    static int toDecimal(String input, int base) {
        input = input.toUpperCase();
        int result = 0, power = 0;
        for (int i = input.length() - 1; i >= 0; i--) {
            char c = input.charAt(i);
            if (Character.isDigit(c)) {
                result += (c - '0') * Math.pow(base, power++);
            }
            else if (Character.isAlphabetic(c)) {
                result += (c - 'A' + 10) * Math.pow(base, power++);
            }
            else {
                throw new IllegalArgumentException("Not a valid number.");
            }
        }
        return result;
    }

    static String toBase(int input, int base) {
        StringBuilder result = new StringBuilder();
        while (input > 0) {
            int digit = input % base;
            input /= base;
            if (digit < 10) {
                result.append(digit);
            }
            else {
                result.append((char)((digit - 10) + 'A'));
            }
        }
        return result.reverse().toString();
    }
}
