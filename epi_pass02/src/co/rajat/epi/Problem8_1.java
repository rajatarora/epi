package co.rajat.epi;

/**
 * Consider two singly linked lists in which each node holds a number. Assume the lists
 * are sorted, i.e., numbers in the lists appear in ascending order within each list. The
 * merge of the two lists is a list consisting of the nodes of the two lists in which numbers
 * appear in ascending order.
 */

class ListNode {
    int data;
    ListNode next;

    public ListNode(int data) {
        this.data = data;
        this.next = null;
    }

    public ListNode(int data, ListNode next) {
        this.data = data;
        this.next = next;
    }
}

public class Problem8_1 {
    public static void main(String[] args) {
        ListNode a5 = new ListNode(10);
        ListNode a4 = new ListNode(8, a5);
        ListNode a3 = new ListNode(6, a4);
        ListNode a2 = new ListNode(4, a3);
        ListNode a1 = new ListNode(2, a2);

        ListNode b5 = new ListNode(9);
        ListNode b4 = new ListNode(7, b5);
        ListNode b3 = new ListNode(5, b4);
        ListNode b2 = new ListNode(3, b3);
        ListNode b1 = new ListNode(1, b2);

        print(a1);
        print(b1);
        print(merge(a1, b1));
    }

    static ListNode merge(ListNode first, ListNode second) {
        ListNode merged = new ListNode(0);
        ListNode iter = merged;
        while (first != null && second != null) {
            if (first.data < second.data) {
                iter.next = first;
                first = first.next;
            }
            else {
                iter.next = second;
                second = second.next;
            }
            iter = iter.next;
        }
        iter.next = (first != null) ? first : second;
        return merged.next;
    }

    static void print(ListNode start) {
        while (start != null) {
            System.out.print(start.data);
            if (start.next != null) {
                System.out.print(" -> ");
            }
            start = start.next;
        }
        System.out.println(System.lineSeparator());
    }
}
