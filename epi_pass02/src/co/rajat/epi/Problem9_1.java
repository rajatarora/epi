package co.rajat.epi;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Design a stack that includes a max operation, in addition to push and pop. The max
 * method should return the maximum value stored in the stack.
 */

class CountNode {
    int max;
    int count;

    public CountNode(int max, int count) {
        this.max = max;
        this.count = count;
    }
}

class StackWithMax {
    private Deque<Integer> stack = new LinkedList<>();
    private Deque<CountNode> frequency = new LinkedList<>();

    public void push(int n) {
        stack.push(n);
        if (frequency.isEmpty()) {
            frequency.push(new CountNode(n, 1));
        }
        else {
            int max = frequency.peek().max;
            if (max < n) {
                frequency.push(new CountNode(n, 1));
            }
            else if (max == n) {
                frequency.peek().count++;
            }
        }
    }

    public int pop() {
        if (stack.isEmpty()) {
            throw new IllegalStateException();
        }
        int n = stack.pop();
        int max = frequency.peek().max;
        if (max == n) {
            frequency.peek().count--;
            if (frequency.peek().count == 0) {
                frequency.pop();
            }
        }
        else if (max < n) {
            frequency.push(new CountNode(n, 1));
        }
        return n;
    }

    public int max() {
        if (stack.isEmpty()) {
            throw new IllegalStateException();
        }
        return frequency.peek().max;
    }
}

public class Problem9_1 {
    public static void main(String[] args) {
        StackWithMax stack = new StackWithMax();
        stack.push(5); System.out.println(stack.max());
        stack.push(8); System.out.println(stack.max());
        stack.push(9); System.out.println(stack.max());
        stack.push(5); System.out.println(stack.max());
        stack.push(9); System.out.println(stack.max());
        stack.pop(); System.out.println(stack.max());
        stack.pop(); System.out.println(stack.max());
        stack.pop(); System.out.println(stack.max());
        stack.pop(); System.out.println(stack.max());
        stack.pop();

    }
}
